# Ejemplo Robot simple (Builder)

En este ejemplo no se implementará el patrón builder completo. 

Solamente añadiremos a una clase los métodos necesarios para poder generar una instancia con los atributos que necesitemos sin la necesidad de contar con un constructor específico para ello.

## Ejecutar código

Ejecutar [MainSimpleBuilder](./MainSimpleBuilder.java)

## Ejecutar pruebas

Los test están ubicados en ``src/main/test/java/``

- [SimpleRobotBuilderTest](./SimpleRobotBuilderTest.java)
