# Builder

Se utiliza para crear objetos complejos, y es especialmente útil cuando se desea que el propio código pueda crear distintas representaciones de estos objetos (evitando constructores complejos).

Más información sobre este patrón en este [enlace](https://refactoring.guru/es/design-patterns/builder)

## Ejemplos

Se han desarrollado dos ejemplos.

- [Robot simple](./examples/simple/robot/README.md): Utiliza la idea del patrón constructor, pero no lo implementa completo. Solo define una forma de construir nuevos objetos, sin tener que definir varios constructores.
- [Robot completo](./examples/full/robot/README.md): Implementación completa del patrón, haciendo uso de una clase ``Director`` que aplicará los ``Builder``.
