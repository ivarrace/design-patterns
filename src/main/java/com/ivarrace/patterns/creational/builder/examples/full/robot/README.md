# Ejemplo Robot completo (Builder)

En este ejemplo vamos a definir dos Builders que implementan la interfaz ``RobotBuilder``. Cada una de estas clases sirve para instanciar un ``Robot`` con atributos distintos (uno será un robot limpiador y otro un robot militar).

Cuando necesitemos una instancia de uno de estos robots, haremos uso del ``Director`` (facilitándole el Builder correspondiente), y este será el encargado de devolvernos dicha instancia.

## Ejecutar código

Ejecutar [MainRobotBuilder](./MainRobotBuilder.java)

## Ejecutar pruebas

Los test están ubicados en ``src/main/test/java/``

- [FullRobotBuilderTest](./FullRobotBuilderTest.java)
