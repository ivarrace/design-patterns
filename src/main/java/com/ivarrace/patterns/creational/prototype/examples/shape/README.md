# Ejemplo Shape (Prototype)

Para este ejemplo se propone un escenario en el que es necesario clonar ciertas formas geométricas.

Para esto vamos a ayudarnos de registro centralizado de prototipos ([ShapeCache](../shape/model/ShapeCache.java)) donde se registraran todos los objetos que podrán ser clonados.

Cuando llamemos al método ``ShapeCache#getShape(id)`` (pasando como parámetro el identificador de la forma que deseamos clonar), nos devolvera una nueva instancia del objeto, que será identico al almacenado en el registro centralizado (mismos valores en los atributos), pero no será la misma instancia.

## Ejecutar código

Ejecutar [MainShapePrototype](./MainShapePrototype.java)

## Ejecutar pruebas

Los test están ubicados en ``src/main/test/java/``

- [PrototypeShapeTest](../../../../../../../../../test/java/com/ivarrace/patterns/creational/prototype/examples/shape/PrototypeShapeTest.java)
