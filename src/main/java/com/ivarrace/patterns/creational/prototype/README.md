# Prototype

Este patrón tiene como finalidad crear nuevos objetos clonando una instancia creada previamente.

Especifica la clase de objetos a crear mediante la clonación de un prototipo que es una instancia ya creada. La clase de los objetos que servirán de prototipo deberá incluir en su interfaz la manera de solicitar una copia, que será desarrollada luego por las clases concretas de prototipos.

El patrón Prototype está disponible en Java listo para usarse con una interfaz ``Cloneable``

Más información sobre este patrón en este [enlace](https://refactoring.guru/es/design-patterns/prototype)

## Ejemplos

- [Shape](./examples/shape/README.md)
