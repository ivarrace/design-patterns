# Ejemplo Lazy (Singleton)

Se define la propia instancia del objeto como atributo de la clase.

````java
private static SingletonLazyClass instance = null;
````

Se añade el método para crear/obtener la ìnstancia

````java
    public static SingletonLazyClass getInstance() {
        if (instance == null) {
            instance = new SingletonLazyClass();
        }
        return instance;
    }
````
**IMPORTANTE**: Esta implementación **no** es segura para trabajar con hilos. Ver el ejemplo [LazyThreadSafe](../lazythreadsafe/README.md)

## Ejecutar código

Ejecutar [MainSingletonLazy](./MainSingletonLazy.java)

## Ejecutar pruebas

Los test están ubicados en ``src/main/test/java/``

- [SingletonLazyTest](../../../../../../../../../test/java/com/ivarrace/patterns/creational/singleton/examples/lazy/SingletonLazyTest.java)
