# Ejemplo Lazy (Singleton)

Se define la propia instancia del objeto como atributo de la clase.

````java
private static SingletonLazyClass instance = null;
````

Se añade el método para crear/obtener la ìnstancia

````java
    public static synchronized SingletonLazyThreadSafeClass getInstance() {
        if (instance == null) {
            instance = new SingletonLazyThreadSafeClass();
        }
        return instance;
    }
````
**IMPORTANTE**: A diferencia de [Lazy Singleton](../lazy/README.md), este método se define como ``synchronized`` para evitar condiciones de carrera.

## Ejecutar código

Ejecutar [MainSingletonThreadSafe](./MainSingletonThreadSafe.java)

## Ejecutar pruebas

Los test están ubicados en ``src/main/test/java/``

- [SingletonLazyThreadSafeTest](../../../../../../../../../test/java/com/ivarrace/patterns/creational/singleton/examples/lazythreadsafe/SingletonLazyThreadSafeTest.java)
