<!-- TODO
Documentación incompleta. Añadir:
¿Hablar sobre el problema que soluciona?
¿Pseudocodigo?
¿Consideraciones antes de aplicar el patrón?...
-->
# Singleton

Garantiza que solo se cree una instancia de un objeto. Para ello, se define el constructor de la clase como privado, impidiendo que se puedan crear nuevas instancias, y añadimos el método ``getInstance()`` para obtener esta instancia única.

Tenemos dos estrategias para instanciar esta clase:
1. En el arranque de la aplicación (Eager), penalizando el rendimiento.
2. Cuando se obtenga la instancia por primera vez (Lazy), evitando la creación de objetos innecesarios. Este método es más recomendable, pero hay que tener cuidado trabajando con hilos por la concurrencia.

Más información sobre este patrón en este [enlace](https://refactoring.guru/es/design-patterns/singleton)

## Ejemplos

- [Eager](./examples/eager/README.md): El objeto se instancia como ``final`` en la propia clase.
- [Lazy](./examples/lazy/README.md): El objeto se instancia en la primera llamada al ``getInstance()``
- [Lazy Thread Safe](./examples/lazythreadsafe/README.md): Al trabajar con hilos evitamos las condiciones de carrera usando ``static synchronized`` para obtener la instancia
