# Ejemplo Eager (Singleton)

Se define la propia instancia del objeto como atributo de la clase y se crea en la misma línea.

````java
private static final SingletonEagerClass INSTANCE = new SingletonEagerClass();
````

Con esto generamos la instancia en el arranque de la aplicación, con las penalizaciones al rendimiento que implica.

Para recuperar la instancia se añade el método
````java
public static SingletonEagerClass getInstance() {
        return INSTANCE;
    }
````

## Ejecutar código

Ejecutar [MainSingletonEager](./MainSingletonEager.java)

## Ejecutar pruebas

Los test están ubicados en ``src/main/test/java/``

- [SingletonEagerTest](../../../../../../../../../test/java/com/ivarrace/patterns/creational/singleton/examples/eager/SingletonEagerTest.java)
