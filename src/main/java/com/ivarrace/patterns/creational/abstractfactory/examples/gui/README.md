<!-- TODO
Completar la ficha del ejemplo
-->

# Ejemplo GUI (Abstract Factory)

Para este ejemplo, ponemos el caso de que necesitamos crear una interfaz gráfica de usuario (GUI).

La interfaz con la que trabajamos tendrá un botón y un checkbox, pero estos variarán en función del SO para el que se necesite la interfaz (siendo distintos para windows, unix...)
Como solución se propone este patrón, definiendo:
- La interfaz ``GuiFactory`` donde se definirán los métodos necesarios para la GUI, y será implementada por ``WindowsFactory`` y ``UnixFactory``, de donde se obtendrán los objetos propios para cada SO.
- Nuestra aplicación principal ``ApplicationGui``, a la que pasaremos por parámetro la factoría correspondiente, y de esta forma la aplicación sabe que trabajará con un botón y un checkbox, pero podrá abstraerse de su tipo (no le importa si es Windows o Unix).

Lo bueno de esto es que si el día de mañana necesitamos adaptarlo a otro SO (por ejemplo necesitamos una GUI para Mac), bastará con crear la factoría correspondiente, sin tener que modificar nada de nuestra aplicación principal, ya que esta nueva factoría hará uso de las interfaces ya definidas.

## Ejecutar código

Ejecutar [MainAbstractFactory](./MainAbstractFactory.java)

## Ejecutar pruebas

Los test están ubicados en ``src/main/test/java/``

- [ApplicationGuiTest](../../../../../../../../../test/java/com/ivarrace/patterns/creational/abstractfactory/examples/gui/ApplicationGuiTest.java)
