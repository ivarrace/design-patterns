<!-- TODO
Documentación incompleta. Añadir:
¿Hablar sobre el problema que soluciona?
¿Pseudocodigo?
¿Consideraciones antes de aplicar el patrón?...
-->
# Abstract Factory

Permite la creación de objetos sin especificar su tipo concreto, delegando esta información a las factorías que definamos.

Más información sobre este patrón en este [enlace](https://refactoring.guru/es/design-patterns/abstract-factory)

## Ejemplos

- [GUI](./examples/gui/README.md)
