# Patrones creacionales

Este tipo de patrones proporcionan diversos mecanismos para la creación de objetos, incrementando la flexibilidad y la reutilización del código existente.

- [**Abstract Factory**](./abstractfactory/README.md): Permite la creación de objetos sin especificar su tipo concreto.
- [**Builder**](./builder/README.md). Se utiliza para crear objetos complejos.
- **Factory Method**. Creates objects without specifying the exact class to create.
- [**Prototype**](./prototype/README.md). Crea un nuevo objeto a partir de un objeto existente.
- [**Singleton**](./singleton/README.md).  Garantiza que solo se cree una instancia de un objeto.